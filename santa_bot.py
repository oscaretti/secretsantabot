from discord.ext import commands
import pickle
import configparser
import discord
import sys
import backend


class SantaBot(commands.Bot):

    def __init__(self, config, pickles):

        parser = configparser.ConfigParser()
        try:
            parser.read(config)
            pref = parser.get("defaults", "prefix")
            token = parser.get("defaults", "token")
        except configparser.NoSectionError:
            with open(config, 'w') as ofile:
                parser["defaults"] = {"prefix": "s!", "token": "<your token goes here>"}
                parser.write(ofile)
            sys.exit("Please fill out the <{}> file".format(config))
        self.token = token

        try:
            with open(pickles, "rb") as ifile:
                self.servers = pickle.load(ifile)
        except (FileNotFoundError, EOFError):
            with open(pickles, "wb") as ofile:
                self.servers = set()
                pickle.dump(self.servers, ofile)

        super().__init__(command_prefix=pref)

    def get_local_server(self, server):
        for s in self.servers:
            if server == s:
                return s
        raise backend.NoServerError


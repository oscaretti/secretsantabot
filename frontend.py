import discord
from discord.ext import commands
import santa_bot
import functools
import backend
import simplecrypt

bot = santa_bot.SantaBot(config="bot_config.ini", pickles="bot.data")
function_names = backend.get_func_names()


# Internal bot methods

def santa_cmd(n, ctx):
    kwargs = backend.get_locale(n)
    return bot.command(pass_context=ctx, **kwargs)


def is_dm(ctx: commands.Context):
    return ctx.guild is None


def typing(func):
    @functools.wraps(func)
    async def inner(ctx: commands.Context, *args, **kwargs):
        with ctx.typing():
            await func(ctx, *args, **kwargs)
    return inner


@bot.event
async def on_ready():
    print("Bot is ready!")


# 'Public' commands available to everyone

name = next(function_names)


@santa_cmd(name, True)
@typing
async def join(ctx: commands.Context, server, password=None, local_name=name):
    if is_dm(ctx):
        try:
            server: backend.Exchange = bot.get_local_server(server)
            server.join(ctx.author, password)
            await ctx.send(backend.get_locale(local_name, 'Success'))
        except backend.BackendError as err:
            await ctx.send(backend.get_locale(local_name, err.get_name()))
        except simplecrypt.DecryptionException:
            await ctx.send(backend.get_locale(local_name, "DecryptionException"))
    else:
        await ctx.send(backend.get_locale(local_name, 'NotDM'))


name = next(function_names)


@santa_cmd(name, True)
@typing
async def leave(ctx: commands.Context, server, local_name=name):
    if is_dm(ctx):
        try:
            server: backend.Exchange = bot.get_local_server(server)
            server.leave(ctx.author)
            await ctx.send(backend.get_locale(local_name, 'Success'))
        except backend.BackendError as err:
            await ctx.send(backend.get_locale(local_name, err.get_name()))
    else:
        await ctx.send(backend.get_locale(local_name, 'NotDM'))


name = next(function_names)


@santa_cmd(name, True)
@typing
async def mycontact(ctx: commands.Context, server, password, *args, local_name=name):
    if is_dm(ctx):
        try:
            server: backend.Exchange = bot.get_local_server(server)
            user: backend.Person = server.get_user(ctx.author)
            user.set_contact(password, *args)
            await ctx.send(backend.get_locale(local_name, 'Success'))
        except backend.BackendError as err:
            await ctx.send(backend.get_locale(local_name, err.get_name()))
        except simplecrypt.DecryptionException:
            await ctx.send(backend.get_locale(local_name, "DecryptionException"))
    else:
        await ctx.send(backend.get_locale(local_name, 'NotDM'))


name = next(function_names)


@santa_cmd(name, True)
@typing
async def mywish(ctx: commands.Context, server, password, *args, local_name=name):
    if is_dm(ctx):
        try:
            server: backend.Exchange = bot.get_local_server(server)
            user: backend.Person = server.get_user(ctx.author)
            user.set_wish(password, *args)
            await ctx.send(backend.get_locale(local_name, 'Success'))
        except backend.BackendError as err:
            await ctx.send(backend.get_locale(local_name, err.get_name()))
        except simplecrypt.DecryptionException:
            await ctx.send(backend.get_locale(local_name, "DecryptionException"))
    else:
        await ctx.send(backend.get_locale(local_name, 'NotDM'))


name = next(function_names)


@santa_cmd(name, True)
@typing
async def passwd(ctx: commands.Context, server, password, local_name=name):
    if is_dm(ctx):
        try:
            server: backend.Exchange = bot.get_local_server(server)
            user: backend.Person = server.get_user(ctx.author)
            user.set_passwd(password)
            await ctx.send(backend.get_locale(local_name, 'Success'))
        except backend.BackendError as err:
            await ctx.send(backend.get_locale(local_name, err.get_name()))
        except simplecrypt.DecryptionException:
            await ctx.send(backend.get_locale(local_name, "DecryptionException"))
    else:
        await ctx.send(backend.get_locale(local_name, 'NotDM'))


name = next(function_names)


@santa_cmd(name, True)
@typing
async def msgsanta(ctx: commands.Context, server, *args, local_name=name):
    if is_dm(ctx):
        try:
            server: backend.Exchange = bot.get_local_server(server)
            recipient: backend.Person = server.get_santa(ctx.author)
            recipient: discord.User = bot.get_user(recipient.id)
            await recipient.send("Message from the person you're to gift:\n{}".format(" ".join(args)))
            await ctx.send(backend.get_locale(local_name, 'Success'))
        except backend.BackendError as err:
            await ctx.send(backend.get_locale(local_name, err.get_name()))
    else:
        await ctx.send(backend.get_locale(local_name, 'NotDM'))


name = next(function_names)


@santa_cmd(name, True)
@typing
async def msgassign(ctx: commands.Context, server, *args, local_name=name):
    if is_dm(ctx):
        try:
            server: backend.Exchange = bot.get_local_server(server)
            recipient: backend.Person = server.get_assignment(ctx.author)
            recipient: discord.User = bot.get_user(recipient.id)
            await recipient.send("Message from your secret santa:\n{}".format(" ".join(args)))
            await ctx.send(backend.get_locale(local_name, 'Success'))
        except backend.BackendError as err:
            await ctx.send(backend.get_locale(local_name, err.get_name()))
    else:
        await ctx.send(backend.get_locale(local_name, 'NotDM'))


name = next(function_names)


@santa_cmd(name, True)
@typing
async def sendinfo(ctx: commands.Context, server, password, local_name=name):
    if is_dm(ctx):
        try:
            server: backend.Exchange = bot.get_local_server(server)
            user: backend.Person = server.get_user(ctx.author)
            santa: backend.Person = server.get_santa(ctx.author)
            contact = user.get_contact(password)
            if contact is None:
                contact = "Not provided"
            wish = user.get_wish(password)
            if wish is None:
                wish = "Not provided"
            other = user.get_other(password)
            if other is None:
                other = "Not provided"
            contact = "This is what your assigned person wises for:\n{}".format(contact)
            wish = "This is what your assigned person wises for:\n{}".format(wish)
            other = "This is what your assigned person wises for:\n{}".format(other)
            santa: discord.User = bot.get_user(santa.id)
            await santa.send("\n\n".join([wish, contact, other]))
        except backend.BackendError as err:
            await ctx.send(backend.get_locale(local_name, err.get_name()))
        except simplecrypt.DecryptionException:
            await ctx.send(backend.get_locale(local_name, "DecryptionException"))
    else:
        await ctx.send(backend.get_locale(local_name, 'NotDM'))


name = next(function_names)


@santa_cmd(name, True)
@typing
async def msgmod(ctx: commands.Context, server=None):
    pass


name = next(function_names)


@santa_cmd(name, True)
@typing
async def blocksanta(ctx: commands.Context, server):
    pass


name = next(function_names)


@santa_cmd(name, True)
@typing
async def blockassign(ctx: commands.Context, server):
    pass


# Below are commands available ony to the moderation

name = next(function_names)


@santa_cmd(name, True)
@typing
async def create(ctx: commands.Context):
    pass


name = next(function_names)


@santa_cmd(name, True)
@typing
async def start(ctx: commands.Context, server):
    pass


name = next(function_names)


@santa_cmd(name, True)
@typing
async def open_joins(ctx: commands.Context, server):
    pass


name = next(function_names)


@santa_cmd(name, True)
@typing
async def close_joins(ctx: commands.Context, server):
    pass


name = next(function_names)


@santa_cmd(name, True)
@typing
async def inspect(ctx: commands.Context, server=None, user=None):
    pass


name = next(function_names)


@santa_cmd(name, True)
@typing
async def msgall(ctx: commands.Context, server, *args):
    pass


name = next(function_names)


@santa_cmd(name, True)
@commands.has_permissions(manage_channels=True)
@typing
async def setmodchnl(ctx: commands.Context, server):
    pass


name = next(function_names)


@santa_cmd(name, True)
@commands.has_permissions(manage_channels=True)
@typing
async def sethelpchnl(ctx: commands.Context, server=None):
    pass


name = next(function_names)


@santa_cmd(name, True)
@commands.has_permissions(manage_channels=True)
@typing
async def swap(ctx: commands.Context, server, first_user, second_user):
    pass


name = next(function_names)


@santa_cmd(name, True)
@commands.has_permissions(manage_channels=True)
@typing
async def kick(ctx: commands.Context, server, user):
    pass


name = next(function_names)


@santa_cmd(name, True)
@commands.has_permissions(manage_channels=True)
@typing
async def ban(ctx :commands.Context, server, user):
    pass


bot.run(bot.token)

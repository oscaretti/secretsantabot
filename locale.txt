[close's error responses]
success = Response to Success form close command
decryptionexception = Response to DecryptionException from close command
permissionerror = Response to PermissionError from close command
notfounderror = Response to NotFoundError from close command
noservererror = Response to NoServerError from close command
stateerror = Response to StateError from close command
bannederror = Response to BannedError from close command
participationerror = Response to ParticipationError from close command
selecterror = Response to SelectError from close command

[close's declaration arguments]
brief = close's declaration arguments's brief
description = close's declaration arguments's description
name = close
help = close's declaration arguments's help
usage = close's declaration arguments's usage

[msgassign's error responses]
success = Response to Success form msgassign command
decryptionexception = Response to DecryptionException from msgassign command
permissionerror = Response to PermissionError from msgassign command
notfounderror = Response to NotFoundError from msgassign command
noservererror = Response to NoServerError from msgassign command
stateerror = Response to StateError from msgassign command
bannederror = Response to BannedError from msgassign command
participationerror = Response to ParticipationError from msgassign command
selecterror = Response to SelectError from msgassign command

[msgassign's declaration arguments]
brief = msgassign's declaration arguments's brief
description = msgassign's declaration arguments's description
name = msgassign
help = msgassign's declaration arguments's help
usage = msgassign's declaration arguments's usage

[msgsanta's error responses]
success = Response to Success form msgsanta command
decryptionexception = Response to DecryptionException from msgsanta command
permissionerror = Response to PermissionError from msgsanta command
notfounderror = Response to NotFoundError from msgsanta command
noservererror = Response to NoServerError from msgsanta command
stateerror = Response to StateError from msgsanta command
bannederror = Response to BannedError from msgsanta command
participationerror = Response to ParticipationError from msgsanta command
selecterror = Response to SelectError from msgsanta command

[msgsanta's declaration arguments]
brief = msgsanta's declaration arguments's brief
description = msgsanta's declaration arguments's description
name = msgsanta
help = msgsanta's declaration arguments's help
usage = msgsanta's declaration arguments's usage

[msgmod's error responses]
success = Response to Success form msgmod command
decryptionexception = Response to DecryptionException from msgmod command
permissionerror = Response to PermissionError from msgmod command
notfounderror = Response to NotFoundError from msgmod command
noservererror = Response to NoServerError from msgmod command
stateerror = Response to StateError from msgmod command
bannederror = Response to BannedError from msgmod command
participationerror = Response to ParticipationError from msgmod command
selecterror = Response to SelectError from msgmod command

[msgmod's declaration arguments]
brief = msgmod's declaration arguments's brief
description = msgmod's declaration arguments's description
name = msgmod
help = msgmod's declaration arguments's help
usage = msgmod's declaration arguments's usage

[ban's error responses]
success = Response to Success form ban command
decryptionexception = Response to DecryptionException from ban command
permissionerror = Response to PermissionError from ban command
notfounderror = Response to NotFoundError from ban command
noservererror = Response to NoServerError from ban command
stateerror = Response to StateError from ban command
bannederror = Response to BannedError from ban command
participationerror = Response to ParticipationError from ban command
selecterror = Response to SelectError from ban command

[ban's declaration arguments]
brief = ban's declaration arguments's brief
description = ban's declaration arguments's description
name = ban
help = ban's declaration arguments's help
usage = ban's declaration arguments's usage

[start's error responses]
success = Response to Success form start command
decryptionexception = Response to DecryptionException from start command
permissionerror = Response to PermissionError from start command
notfounderror = Response to NotFoundError from start command
noservererror = Response to NoServerError from start command
stateerror = Response to StateError from start command
bannederror = Response to BannedError from start command
participationerror = Response to ParticipationError from start command
selecterror = Response to SelectError from start command

[start's declaration arguments]
brief = start's declaration arguments's brief
description = start's declaration arguments's description
name = start
help = start's declaration arguments's help
usage = start's declaration arguments's usage

[join's error responses]
success = Response to Success form join command
decryptionexception = Response to DecryptionException from join command
permissionerror = Response to PermissionError from join command
notfounderror = Response to NotFoundError from join command
noservererror = Response to NoServerError from join command
stateerror = Response to StateError from join command
bannederror = Response to BannedError from join command
participationerror = Response to ParticipationError from join command
selecterror = Response to SelectError from join command

[join's declaration arguments]
brief = join's declaration arguments's brief
description = join's declaration arguments's description
name = join
help = join's declaration arguments's help
usage = join's declaration arguments's usage

[passwd's error responses]
success = Response to Success form passwd command
decryptionexception = Response to DecryptionException from passwd command
permissionerror = Response to PermissionError from passwd command
notfounderror = Response to NotFoundError from passwd command
noservererror = Response to NoServerError from passwd command
stateerror = Response to StateError from passwd command
bannederror = Response to BannedError from passwd command
participationerror = Response to ParticipationError from passwd command
selecterror = Response to SelectError from passwd command

[passwd's declaration arguments]
brief = passwd's declaration arguments's brief
description = passwd's declaration arguments's description
name = passwd
help = passwd's declaration arguments's help
usage = passwd's declaration arguments's usage

[modchnl's error responses]
success = Response to Success form modchnl command
decryptionexception = Response to DecryptionException from modchnl command
permissionerror = Response to PermissionError from modchnl command
notfounderror = Response to NotFoundError from modchnl command
noservererror = Response to NoServerError from modchnl command
stateerror = Response to StateError from modchnl command
bannederror = Response to BannedError from modchnl command
participationerror = Response to ParticipationError from modchnl command
selecterror = Response to SelectError from modchnl command

[modchnl's declaration arguments]
brief = modchnl's declaration arguments's brief
description = modchnl's declaration arguments's description
name = modchnl
help = modchnl's declaration arguments's help
usage = modchnl's declaration arguments's usage

[mywish's error responses]
success = Response to Success form mywish command
decryptionexception = Response to DecryptionException from mywish command
permissionerror = Response to PermissionError from mywish command
notfounderror = Response to NotFoundError from mywish command
noservererror = Response to NoServerError from mywish command
stateerror = Response to StateError from mywish command
bannederror = Response to BannedError from mywish command
participationerror = Response to ParticipationError from mywish command
selecterror = Response to SelectError from mywish command

[mywish's declaration arguments]
brief = mywish's declaration arguments's brief
description = mywish's declaration arguments's description
name = mywish
help = mywish's declaration arguments's help
usage = mywish's declaration arguments's usage

[swap's error responses]
success = Response to Success form swap command
decryptionexception = Response to DecryptionException from swap command
permissionerror = Response to PermissionError from swap command
notfounderror = Response to NotFoundError from swap command
noservererror = Response to NoServerError from swap command
stateerror = Response to StateError from swap command
bannederror = Response to BannedError from swap command
participationerror = Response to ParticipationError from swap command
selecterror = Response to SelectError from swap command

[swap's declaration arguments]
brief = swap's declaration arguments's brief
description = swap's declaration arguments's description
name = swap
help = swap's declaration arguments's help
usage = swap's declaration arguments's usage

[msgall's error responses]
success = Response to Success form msgall command
decryptionexception = Response to DecryptionException from msgall command
permissionerror = Response to PermissionError from msgall command
notfounderror = Response to NotFoundError from msgall command
noservererror = Response to NoServerError from msgall command
stateerror = Response to StateError from msgall command
bannederror = Response to BannedError from msgall command
participationerror = Response to ParticipationError from msgall command
selecterror = Response to SelectError from msgall command

[msgall's declaration arguments]
brief = msgall's declaration arguments's brief
description = msgall's declaration arguments's description
name = msgall
help = msgall's declaration arguments's help
usage = msgall's declaration arguments's usage

[kick's error responses]
success = Response to Success form kick command
decryptionexception = Response to DecryptionException from kick command
permissionerror = Response to PermissionError from kick command
notfounderror = Response to NotFoundError from kick command
noservererror = Response to NoServerError from kick command
stateerror = Response to StateError from kick command
bannederror = Response to BannedError from kick command
participationerror = Response to ParticipationError from kick command
selecterror = Response to SelectError from kick command

[kick's declaration arguments]
brief = kick's declaration arguments's brief
description = kick's declaration arguments's description
name = kick
help = kick's declaration arguments's help
usage = kick's declaration arguments's usage

[inspect's error responses]
success = Response to Success form inspect command
decryptionexception = Response to DecryptionException from inspect command
permissionerror = Response to PermissionError from inspect command
notfounderror = Response to NotFoundError from inspect command
noservererror = Response to NoServerError from inspect command
stateerror = Response to StateError from inspect command
bannederror = Response to BannedError from inspect command
participationerror = Response to ParticipationError from inspect command
selecterror = Response to SelectError from inspect command

[inspect's declaration arguments]
brief = inspect's declaration arguments's brief
description = inspect's declaration arguments's description
name = inspect
help = inspect's declaration arguments's help
usage = inspect's declaration arguments's usage

[open's error responses]
success = Response to Success form open command
decryptionexception = Response to DecryptionException from open command
permissionerror = Response to PermissionError from open command
notfounderror = Response to NotFoundError from open command
noservererror = Response to NoServerError from open command
stateerror = Response to StateError from open command
bannederror = Response to BannedError from open command
participationerror = Response to ParticipationError from open command
selecterror = Response to SelectError from open command

[open's declaration arguments]
brief = open's declaration arguments's brief
description = open's declaration arguments's description
name = open
help = open's declaration arguments's help
usage = open's declaration arguments's usage

[helpchnl's error responses]
success = Response to Success form helpchnl command
decryptionexception = Response to DecryptionException from helpchnl command
permissionerror = Response to PermissionError from helpchnl command
notfounderror = Response to NotFoundError from helpchnl command
noservererror = Response to NoServerError from helpchnl command
stateerror = Response to StateError from helpchnl command
bannederror = Response to BannedError from helpchnl command
participationerror = Response to ParticipationError from helpchnl command
selecterror = Response to SelectError from helpchnl command

[helpchnl's declaration arguments]
brief = helpchnl's declaration arguments's brief
description = helpchnl's declaration arguments's description
name = helpchnl
help = helpchnl's declaration arguments's help
usage = helpchnl's declaration arguments's usage

[blockassign's error responses]
success = Response to Success form blockassign command
decryptionexception = Response to DecryptionException from blockassign command
permissionerror = Response to PermissionError from blockassign command
notfounderror = Response to NotFoundError from blockassign command
noservererror = Response to NoServerError from blockassign command
stateerror = Response to StateError from blockassign command
bannederror = Response to BannedError from blockassign command
participationerror = Response to ParticipationError from blockassign command
selecterror = Response to SelectError from blockassign command

[blockassign's declaration arguments]
brief = blockassign's declaration arguments's brief
description = blockassign's declaration arguments's description
name = blockassign
help = blockassign's declaration arguments's help
usage = blockassign's declaration arguments's usage

[blocksanta's error responses]
success = Response to Success form blocksanta command
decryptionexception = Response to DecryptionException from blocksanta command
permissionerror = Response to PermissionError from blocksanta command
notfounderror = Response to NotFoundError from blocksanta command
noservererror = Response to NoServerError from blocksanta command
stateerror = Response to StateError from blocksanta command
bannederror = Response to BannedError from blocksanta command
participationerror = Response to ParticipationError from blocksanta command
selecterror = Response to SelectError from blocksanta command

[blocksanta's declaration arguments]
brief = blocksanta's declaration arguments's brief
description = blocksanta's declaration arguments's description
name = blocksanta
help = blocksanta's declaration arguments's help
usage = blocksanta's declaration arguments's usage

[sendinfo's error responses]
success = Response to Success form sendinfo command
decryptionexception = Response to DecryptionException from sendinfo command
permissionerror = Response to PermissionError from sendinfo command
notfounderror = Response to NotFoundError from sendinfo command
noservererror = Response to NoServerError from sendinfo command
stateerror = Response to StateError from sendinfo command
bannederror = Response to BannedError from sendinfo command
participationerror = Response to ParticipationError from sendinfo command
selecterror = Response to SelectError from sendinfo command

[sendinfo's declaration arguments]
brief = sendinfo's declaration arguments's brief
description = sendinfo's declaration arguments's description
name = sendinfo
help = sendinfo's declaration arguments's help
usage = sendinfo's declaration arguments's usage

[leave's error responses]
success = Response to Success form leave command
decryptionexception = Response to DecryptionException from leave command
permissionerror = Response to PermissionError from leave command
notfounderror = Response to NotFoundError from leave command
noservererror = Response to NoServerError from leave command
stateerror = Response to StateError from leave command
bannederror = Response to BannedError from leave command
participationerror = Response to ParticipationError from leave command
selecterror = Response to SelectError from leave command

[leave's declaration arguments]
brief = leave's declaration arguments's brief
description = leave's declaration arguments's description
name = leave
help = leave's declaration arguments's help
usage = leave's declaration arguments's usage

[mycontact's error responses]
success = Response to Success form mycontact command
decryptionexception = Response to DecryptionException from mycontact command
permissionerror = Response to PermissionError from mycontact command
notfounderror = Response to NotFoundError from mycontact command
noservererror = Response to NoServerError from mycontact command
stateerror = Response to StateError from mycontact command
bannederror = Response to BannedError from mycontact command
participationerror = Response to ParticipationError from mycontact command
selecterror = Response to SelectError from mycontact command

[mycontact's declaration arguments]
brief = mycontact's declaration arguments's brief
description = mycontact's declaration arguments's description
name = mycontact
help = mycontact's declaration arguments's help
usage = mycontact's declaration arguments's usage

